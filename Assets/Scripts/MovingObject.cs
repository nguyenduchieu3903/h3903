using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public float speed;
    public Bird bird123;
    
    // Start is called before the first frame update
    void Start()
    {
        RandomPosition();
    }

    // Update is called once per frame
    void Update()
    {
        if (!bird123.isStart) 
            return;

        transform.position -= new Vector3(speed, 0, 0);
        if (transform.position.x < -6)
        {
            Vector3 m_CurrentPosition = transform.position;
            m_CurrentPosition.x = 5;
            transform.position = m_CurrentPosition;

            RandomPosition();
        }

        if (bird123.m_Score >= 5)
        {
            transform.position -= new Vector3( 0.5f, 0, 0) * Time.deltaTime;
        }
        if (bird123.m_Score >= 10)
        {
            transform.position -= new Vector3( 1f, 0, 0) * Time.deltaTime;
        }
        if (bird123.m_Score >= 15)
        {
            transform.position -= new Vector3(1.5f, 0, 0) * Time.deltaTime;
        }
        if (bird123.m_Score >= 20)
        {
            transform.position -= new Vector3(2f, 0, 0) * Time.deltaTime;
        }
    }
    
    void RandomPosition()
    {
        Vector3 m_CurrentPosition = transform.position;
        m_CurrentPosition.y = Random.Range(2.0f, -2.0f);
        transform.position = m_CurrentPosition;
    }
}
