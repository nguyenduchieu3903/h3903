using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bird : MonoBehaviour
{
    public float force;
    public Rigidbody2D birdBody;
    public TextMesh textScore, textGameOver, textGameStart, textGameRetry;
    public Animator animator;
    public int m_Score;
    public bool isStart;

    private bool m_isDie;
    private float m_tempDelaytime;
    
    // Start is called before the first frame update
    void Start()
    {
        m_Score = 0;
        isStart= false;
        textScore.text = m_Score.ToString();
        textGameOver.gameObject.SetActive(false);
        textGameRetry.gameObject.SetActive(false);
        textGameStart.gameObject.SetActive(true);

        animator.SetInteger("HP", 1);
        m_isDie = false;
        Debug.Log("Start");
    }

    // Update is called once per frame
    void Update()
    {
        if (!isStart)
        {
            isStart = true;
            birdBody.simulated = true;
        }
        else
        {
            if (m_isDie)
            {
                m_tempDelaytime -= Time.deltaTime;
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (m_isDie)
                {
                    if (m_tempDelaytime <= 0)
                        Reloadgame();
                }
                else
                    textGameStart.gameObject.SetActive(false);
                    birdBody.velocity = new Vector3(0, force, 0);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Building")
        {
            //Destroy(gameObject);
            animator.SetInteger("HP", 0);

            m_isDie= true;
            m_tempDelaytime = 2;

            textScore.gameObject.SetActive(false);
            textGameOver.gameObject.SetActive(true);
            textGameRetry.gameObject.SetActive(true);
            textGameStart.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Building")
        {

            m_Score++;
            textScore.text = m_Score.ToString();
            Debug.Log("Score " + m_Score);
        }
    }

    public void Reloadgame()
    {
        SceneManager.LoadScene(0);
    }
}
